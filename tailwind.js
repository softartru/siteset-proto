module.exports = {
  purge: [
    './public/**/*.html',
    './public/**/*.php',
  ],

  theme: {
    extend: {
      colors: {
        brown: {
          100: '#f6f3f4',
          200: '#ede9ea',
          300: '#d0c9cb',
          400: '#c2b5b6',
          500: '#9a8e92',
          600: '#867077',
          700: '#5c494f',
          800: '#4a383e',
          900: '#080808',
        },
      },

      fontFamily: {
        body: ['Gotham Pro', 'sans-serif'],
        heading: ['RoadRadio', 'sans-serif'],
      },

      fontSize: {
        '26': '26px',
      },

      borderRadius: {
        '20': '20px',
        'xl': '0.75rem',
        '2xl': '1rem',
        '3xl': '1.5rem',
      },

      borderWidth: {
        '5': '5px',
      },

      boxShadow: {
        '1': '0 4px 13px rgba(150, 131, 137, 1)',
        '2': '0 7px 29px rgba(150, 131, 137, 1)',
      },
    },
  },
  variants: {},
  plugins: [],
}
