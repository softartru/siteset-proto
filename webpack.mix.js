const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

require('laravel-mix-tailwind');

mix
	.version()
	.setPublicPath('./')
	.tailwind()
	.sass('src/sass/app.scss', 	'public/css')
	.scripts([
		'node_modules/jquery/dist/jquery.js',
		'node_modules/owl.carousel/src/js/owl.carousel.js',
		'src/js/siteset.win.js',
		'src/js/siteset.scrollbar.js',
		'src/js/mobilemenu.js',
		'src/js/siteset.gallery.js',
		'src/js/jquery-ui.min.js',
		'src/js/theme-script.js',
		'src/js/accordion.js',

		/*
		'node_modules/jquery/dist/jquery.js',
		'node_modules/vue/dist/vue.min.js',
		'node_modules/jquery-datetimepicker/build/jquery.datetimepicker.full.min.js',
		'node_modules/jquery-mask-plugin/dist/jquery.mask.min.js',
		'resources/js/site/jquery.cookie.js',
		'node_modules/popper.js/dist/umd/popper.min.js',
		'node_modules/bootstrap/dist/js/bootstrap.min.js',
		'resources/js/site/siteset.filterslider.js',
		'resources/js/site/owl.carousel.min.js',
		'resources/js/site/siteset.win.js',
		'resources/js/site/mobilemenu.js',
		'resources/js/site/siteset.gallery.js',
		'resources/js/site/maxheight.js',
		'resources/js/site/jquery.parallax-1.1.3.js',
		'resources/js/site/parallaxInit.js',
		'resources/js/site/siteset.rate.js',
		'resources/js/site/siteset.sitemap.js',
		'resources/js/site/siteset.policy.js',
		'resources/js/site/siteset.navigator.js',
		'resources/js/site/siteset.scrollbar.js',
		'resources/js/site/siteset.cart.js',
		'resources/js/site/siteset.search.js',
		'resources/js/site/ymap.js',
		'resources/js/site/siteset.scroll.js',
		'resources/js/site/siteset.product.navigator.js',
		'resources/js/site/siteset.filter.js',
		'vendor/siteset/recaptcha/js/siteset.recaptcha.js',
		'resources/js/site/theme-script.js'
		*/
		],
		'public/js/app.js'
	);

