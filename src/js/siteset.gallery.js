/**
 * @name SiteSet Gallery JS
 * @version 1.0.2
 * @author Alexey Glumov
 * @copyright Copyright (c) 2016 SoftArt Internet Company http://softart.ru
 */

/**
 * глобальная переменная
 * галереи
 *
 * @var sitesetGallery
 */

var sitesetGalleryList;

/**
 * функция доступа к объект
 * галереи
 *
 * @return sitesetGallery
 *
 * @param boolean reload перезагрузить галерею
 */

function ssGallery(reload)
{
	if (undefined === sitesetGalleryList || true === reload)
		sitesetGalleryList = new sitesetGallery();

	return sitesetGalleryList;
}

function sitesetGallery()
{
	/**
	 * текущее положение курсора на галерее
	 *
	 * @var - integer
	 */

	this.cursor = 0;

	/**
	 * скорость появления окон
	 *
	 * @var - integer
	 */

	this.speed   = 300;

	/**
	 * прозрачность фона
	 *
	 * @var - integer/float
	 */

	this.opacity = 0.8;

	/**
	 * список галерей
	 *
	 * @var - array
	 */

	this.gList = [];

	/**
	 * метод собирает изображения
	 *
	 */

	this.init = function() {

		// удаляем вертску если она есть
		if ($('.ss-gallery__loader').length > 0)
			$('.ss-gallery__loader').remove();

		if ($('.ss-gallery__modal-bg').length > 0)
			$('.ss-gallery__modal-bg').remove();

		if ($('.ss-gallery__modal').length > 0)
			$('.ss-gallery__modal').remove();

		$('.ss-gallery')	.unbind('click');
		$('.ss-gallery a')	.unbind('click');

		// создаем верстку
		$('body').append(''
			+ '<div class="ss-gallery__loader"></div>'
			+ '<div class="ss-gallery__modal-bg"></div>'

			+ '<div class="ss-gallery__modal">'

				+ '<div class="ss-gallery__desc"></div>'
				+ '<div class="ss-gallery__close"></div>'

				+ '<div class="ss-gallery__switch ss-gallery__switch-left">'
					+ '<div class="ss-gallery__switch-left-ico"></div>'
				+ '</div>'
				+ '<div class="ss-gallery__switch ss-gallery__switch-right">'
					+ '<div class="ss-gallery__switch-right-ico"></div>'
				+ '</div>'

				+ '<div class="ss-gallery__img"></div>'
				+ '<div class="ss-gallery__count"></div>'
			+ '</div>'
		);

		// событие на закрытие
		$('.ss-gallery__modal-bg, .ss-gallery__close').bind('click', function () {
			sitesetGalleryList.close();
		});

		// событие на листание
		$('.ss-gallery__switch-left').bind('click', function () {
			sitesetGalleryList.prev();
		});

		$('.ss-gallery__switch-right').bind('click', function () {
			sitesetGalleryList.next();
		});
	};

	/**
	 * создание слушателя событий
	 *
	 * @param jq box блок события
	 * @param string event имя события
	 * @param function callback функция замыкания
	 */

	this.trigger = function (box, event, callback) {
		box.bind(event, callback);
	};

	/**
	 * метод открывает галерею
	 *
	 */

	this.open = function() {
		var self = this;

		// загружаем изображение
		this.load();

		// поднять фон
		$('.ss-gallery__modal-bg').fadeTo(self.speed, self.opacity, function () {
			// поднять модальное окно
			$('.ss-gallery__modal').fadeTo(self.speed, 1, function () {

				// слушаем стрелки влево и вправо и esc
				$(window).bind('keydown', function(e) {
					switch (e.keyCode) {
						case 27:
							sitesetGalleryList.close();
							break;

						case 37:
							sitesetGalleryList.prev();
							break;

						case 39:
							sitesetGalleryList.next();
							break;
					}
				});

			});
		});
	};

	/**
	 * метод загружает фото
	 *
	 */

	this.load = function() {
		// расчитываем скролл
		var scrTop = 0;

		//определяем позицию скролла всех браузеров кроме гугла
		scrTop = $("html,body").scrollTop();

		//определяем позицию скролла у гугл хрома
		if(scrTop == 0)
			scrTop = $("body").scrollTop();

		// определяем сдвиг, а это высота экрана - высота изображения / 2
		scrTop += ( ($(window).height() - this.gList[this.cursor].getImg().height) / 2);

		$('.ss-gallery__modal').css({
			'top':         scrTop + 'px',
			'width':       this.gList[this.cursor].getImg().width,
			'height':      this.gList[this.cursor].getImg().height,
			'left':        '50%',
			'margin-left': '-' + (this.gList[this.cursor].getImg().width / 2) + 'px'
		});

		// загружаем изображение
		$('.ss-gallery__img').html(''
			+ '<img'
				+' src="' + this.gList[this.cursor].getImg().link  + '"'
				+' alt="' + this.gList[this.cursor].getImg().title + '"'
			+ '>'
		);

		// формируем заголовок
		this.getTitle();

		// формируем кнопки влево вправо
		this.getSwitch();

		// событие "изображение подгрузилось"
		this.gList[this.cursor].obj.trigger('ss.loaded.image.gallery', [this.gList[this.cursor]]);
	};

	/**
	 * метод формирует кнопки перехода
	 *
	 */

	this.getSwitch = function() {
		var self = this;

		// если в галереи одно изображение
		if (self.gList[self.cursor].qnt == 1) {
			// прчем кнопки
			$('.ss-gallery__switch').css('display', 'none');

			// выходим
			return;
		}

		// показываем кнопки
		$('.ss-gallery__switch').css('display', '');
	};

	/**
	 * метод формирует заголовок
	 *
	 */

	this.getTitle = function() {
		$('.ss-gallery__desc') .text(this.gList[this.cursor].getImg().title);

		// показываем кол-во
		$('.ss-gallery__count').css('display', '');

		// если в галереи более одного изображения
		if (this.gList[this.cursor].qnt > 1)
			// заполняем информер
			$('.ss-gallery__count').text(''
				+ 'фото '
				+ (this.gList[this.cursor].cursor + 1)
					+ ' из '
				+ this.gList[this.cursor].qnt)
			;

		// в противном случае
		else
			// прячем счетчик
			$('.ss-gallery__count').css('display', 'none');
	};

	/**
	 * метод закрывает галерею
	 *
	 */

	this.close = function() {
		var self = this;

		// прячем все
		$('.ss-gallery__modal').fadeTo(self.speed, 0, function () {
			$('.ss-gallery__modal').css('display', '');

			$('.ss-gallery__modal-bg').fadeTo(self.speed, 0, function () {
				$('.ss-gallery__modal-bg').css('display', '');

				// слушаем стрелки влево и вправо
				$(window).unbind('keydown');
			});
		});
	};

	/**
	 * метод получает след изображение
	 *
	 */

	this.next = function() {
		var self = this;

		// если в галерее одно изображение
		if (self.gList[self.cursor].qnt == 1)
			// выходим, листать нечего
			return;

		// если уже происходит переход
		if ($('.ss-gallery__modal').hasClass('ss-gallery__busy'))
			// выходим
			return;

		// ставим флаг, позволяющий пресечь попытку
		// запустить переход второй раз
		$('.ss-gallery__modal').addClass('ss-gallery__busy');

		// спрятать модальное окно
		$('.ss-gallery__modal').fadeTo(self.speed, 0, function () {
			// переключаем изображение
			self.gList[self.cursor].next();

			// загружаем
			self.load()

			// поднять модальное окно
			$('.ss-gallery__modal').fadeTo(self.speed, 1, function() {
				// удаляем флаг
				$('.ss-gallery__modal').removeClass('ss-gallery__busy');
			});
		});
	};

	/**
	 * метод получает предыдущее изображение
	 *
	 */

	this.prev = function() {
		var self = this;

		// если в галерее одно изображение
		if (self.gList[self.cursor].qnt == 1)
			// выходим, листать нечего
			return;

		// если уже происходит переход
		if ($('.ss-gallery__modal').hasClass('ss-gallery__busy'))
			// выходим
			return;

		// ставим флаг, позволяющий пресечь попытку
		// запустить переход второй раз
		$('.ss-gallery__modal').addClass('ss-gallery__busy');

		// спрятать модальное окно
		$('.ss-gallery__modal').fadeTo(self.speed, 0, function () {
			// переключаем изображение
			self.gList[self.cursor].prev();

			// загружаем
			self.load()

			// поднять модальное окно
			$('.ss-gallery__modal').fadeTo(self.speed, 1, function() {
				// удаляем флаг
				$('.ss-gallery__modal').removeClass('ss-gallery__busy');
			});
		});
	};

	/**
	 * метод собирает список изображений
	 *
	 */

	this.gatherGList = function() {
		var self = this;

		$('.ss-gallery').each(function() {
			var obj = new sitesetGalleryImg({
					'box':   $(this),
					'index': self.gList.length
				});

			if (!self.empty(obj.imgList)) {
				obj.setStack();
				self.gList.push(obj);
			}
		});
	};

	/**
	 * метод выбирает галерею
	 *
	 * @return this
	 *
	 * @param integer index - индекс галереи
	 */

	this.setIndex = function(index) {
		this.cursor = index;
		return this;
	};

	/**
	 * метод устанавливает курсор на изображение
	 *
	 * @return this
	 *
	 * @param integer cursor - индекс изображения
	 */

	this.setCursor = function(cursor) {
		this.gList[this.cursor].cursor = cursor;
		return this;
	};

	/**
	 * метод проверят переменную на пустоту
	 *
	 * @return - boolean
	 *
	 * @param ??? variable - переменная для проверки
	 */

	this.empty = function (variable) {

		if (typeof(variable) == 'object') {
			for (i in variable)
				return false;

			return true;
		}

		else if (typeof(variable) == 'number') {
			return (variable == 0) ? true : false;
		}

		else if (typeof(variable) == 'string') {
			return (variable == '' || variable == undefined) ? true : false;
		}

		return true;
	};

	// формируем верстку галереи
	this.init();

	// собираем галереи
	this.gatherGList();
}

/**
 * класс управления изображениями галереи
 *
 */

function sitesetGalleryImg(opt)
{
	/**
	 * объект галереи
	 *
	 * @var - object
	 */

	this.obj = opt.box;

	/**
	 * ключ в глобальном массиве галереи
	 *
	 * @var - intrger
	 */

	this.index = opt.index;

	/**
	 * кол-во изображений в галерее
	 *
	 * @var - integer
	 */

	this.qnt = 0;

	/**
	 * текущее положение курсора на изображении
	 *
	 * @var - integer
	 */

	this.cursor = 0;

	/**
	 * список изображений
	 *
	 * @var - array
	 */

	this.imgList = [];

	/**
	 * метод собирает список изображений
	 *
	 */

	this.gatherImgList = function() {
		var self = this;
		var img  = {};

		// если прислан не блок с списком изображений
		// а лишь одно изображение
		if (self.obj[0].tagName == 'A') {
			// если атрибут href не пуст
			if (!self.empty(self.obj.attr('href'))) {
				// создаем объект с изображением
				cursor    = self.imgList.length;

				img       = {};
				img.link  = self.obj.attr('href');
				img.title = self.obj.find('img').attr('alt');

				//self.obj.prop('href', 'javascript: void(0);');
				self.obj.bind('click', function () {
					sitesetGalleryList.setIndex(self.index).setCursor(cursor).open();
					return false;
				});

				self.imgList.push(img);
			}
		}

		// если прислан блок с изображениями
		else
			self.obj.find("a").each(function() {
				if (self.empty($(this).attr('href')))
					return;

				// создаем объект с изображением
				var cursor = self.imgList.length;

				img        = {};
				img.link   = $(this).attr('href');
				img.title  = $(this).find('img').attr('alt');

				//$(this).prop('href', 'javascript: void(0);');
				$(this).bind('click', function () {
					sitesetGalleryList.setIndex(self.index).setCursor(cursor).open();
					return false;
				});

				self.imgList.push(img);
			});

		self.qnt = self.imgList.length;
	};

	/**
	 * метод получает изображение
	 *
	 */

	this.getImg = function () {
		var self = this;

		var wW   = $(window).width()  - 100;
		var hW   = $(window).height() - 100;

		var wI   = $('.ss-gallery__g-' + self.index + '-' + self.cursor).width();
		var hI   = $('.ss-gallery__g-' + self.index + '-' + self.cursor).height();

		// уравниваем изображение по экрану
		while (true) {
			// если изображение не впределах окна
			if (wW < wI || hW < hI) {
				// определяем что вышло за экран
				// если ширина картинки вышла за экран
				if (wW < wI) {
					// определяем новую высоту
					hI = wW / (wI / hI);
					// заполняем ширину
					wI = wW;
				}

				// значит за основу берем высоту
				else {
					// расчитываем ширину
					wI = hW / (hI / wI);
					// заполняем высоту
					hI = hW;
				}
			}
			else
				break;
		}

		return {
			'width':    wI,
			'height':   hI,
			'link':     self.imgList[self.cursor].link,
			'title':    self.imgList[self.cursor].title,
			'position': self.cursor + 1
		};
	};

	/**
	 * метод выберает следующее изображение
	 *
	 */

	this.next = function () {
		var self = this;

		if (!self.empty(self.imgList[(self.cursor + 1)]))
			self.cursor = self.cursor + 1;
		else
			self.cursor = 0;
	};

	/**
	 * метод выбирает предыдущее изображение
	 *
	 */

	this.prev = function () {
		var self = this;

		if (!self.empty(self.imgList[(self.cursor - 1)]))
			self.cursor = self.cursor - 1;
		else
			self.cursor = self.imgList.length - 1;
	};

	/**
	 * метод заполняет стек изображений
	 *
	 */

	this.setStack = function () {
		var self = this;

		// листаем изображения
		for (i in self.imgList)
			$('.ss-gallery__loader').append(''
				+ '<img'
					+ ' class="ss-gallery__g-' + self.index + '-' + i + '"'
					+ ' src="' + self.imgList[i].link  + '"'
					+ ' alt="' + self.imgList[i].title + '"'
				+'>'
			);
	};

	/**
	 * метод проверят переменную на пустоту
	 *
	 * @return - boolean
	 *
	 * @param ??? variable - переменная для проверки
	 */

	this.empty = function (variable) {

		if (typeof(variable) == 'object') {
			for (i in variable)
				return false;

			return true;
		}

		else if (typeof(variable) == 'number') {
			return (variable == 0) ? true : false;
		}

		else if (typeof(variable) == 'string') {
			return (variable == '' || variable == undefined) ? true : false;
		}

		return true;
	};

	// собираем изображения
	this.gatherImgList();
}

/**
 * запуск галереи
 *
 */

$(document).ready(function() {
	ssGallery();
});