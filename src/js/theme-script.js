(function ($) {
	"use strict";

	/* ---------------------------------------------
	 Scripts ready
	 --------------------------------------------- */
	$(document).ready(function () {

		// scroll top  
		$(document).on('click', '.b-scroll__top', function () {
			$('body,html').animate({ scrollTop: 0 }, 400);
			return false;
		})

		// view grid list product 
		$(document).on('click', '.b-product-view .view-as-grid', function () {
			$(this).closest('.b-product-view').find('.b-product-view__item').removeClass('selected');
			$(this).addClass('selected');
			$(this).closest('#view-product-list').find('.b-product-list').css('display', 'none');
			$(this).closest('#view-product-list').find('.b-product-table').css('display', 'none');
			$(this).closest('#view-product-list').find('.b-product-grid').css('display', 'flex');

			return false;
		})

		// view list list product 
		$(document).on('click', '.b-product-view .view-as-list', function () {
			$(this).closest('.b-product-view').find('.b-product-view__item').removeClass('selected');
			$(this).addClass('selected');
			$(this).closest('#view-product-list').find('.b-product-grid').css('display', 'none');
			$(this).closest('#view-product-list').find('.b-product-table').css('display', 'none');
			$(this).closest('#view-product-list').find('.b-product-list').css('display', 'block');

			return false;
		})

		// view table list product 
		$(document).on('click', '.b-product-view .view-as-table', function () {
			$(this).closest('.b-product-view').find('.b-product-view__item').removeClass('selected');
			$(this).addClass('selected');
			$(this).closest('#view-product-list').find('.b-product-grid').css('display', 'none');
			$(this).closest('#view-product-list').find('.b-product-list').css('display', 'none');
			$(this).closest('#view-product-list').find('.b-product-table').css('display', 'block');

			return false;
		})

		// owl-carousel слайдер на главной
		$(".b-deals__carousel").owlCarousel({
			margin: 0,
			loop: true,
			nav: false,
			navText: ['&#8249;', '&#8250;'],
			dots: true,
			items: 1,
			autoplay: true,
			animateOut: 'fadeOut',
			//smartSpeed:1000,
			autoplayTimeout: 3000
		})

		// owl-carousel отзывы на главной
		$(".b-review__carousel").owlCarousel({
			margin: 0,
			loop: false,
			nav: false,
			dots: true,
			items: 1,
			autoplay: false,
		})

		// owl-carousel детализация товара - изображения
		$(".b-shop-product__carousel-image").owlCarousel({
			margin: 10,
			//loop:true,
			nav: true,
			navText: ['&#8249;', '&#8250;'],
			dots: false,
			items: 3
		})

		// owl-carousel детализация - другие товары
		$(".b-shop-product__carousel").owlCarousel({
			margin: 24,
			//loop:true,
			nav: true,
			navText: ['<i>&#8249;</i>', '<i>&#8250;</i>'],
			dots: false,
			responsive: {
				0: {
					items: 1
				},
				640: {
					items: 2
				},

				768: {
					items: 3
				},

				1024: {
					items: 4
				}
			}
		})

		// горизонтальное меню категорий продукции для мобилы в шапке
		$(".b-horizont-menu__carousel").owlCarousel({
			margin: 20,
			autoWidth: true,
			//loop:true,
			nav: true,
			navText: ['<img src="images/prev.jpg">', '<img src="images/next.jpg">'],
			dots: false,
			items: 2
		})

		// owl-carousel теги каталог-рубрика
		$(".b-tags").owlCarousel({
			margin: 0,
			//loop:true,
			nav: true,
			navText: ['&#8249;', '&#8250;'],
			dots: false,
			autoWidth: true
		})

		$('.b-sidebar__nav-item').hover(function () {
			$('.b-sidebar__nav-name').css('display', 'block');
		},
			function () {
				$('.b-sidebar__nav-name').css('display', 'none');
			});

		// это только для того, чтобы показать, что "окошко живого поиска" прописано.
		$('.s-search-box').find('input').bind('focus', function () {
			$('.ss-result-search').css('display', 'block');
		});

		$('.s-search-box').find('input').bind('blur', function () {
			$('.ss-result-search').css('display', 'none');
		});

		// фильтр для мобилы - надо переделать, чтобы для вариантов > 767 .b-section-menu был открыт
		$(".b-section-menu-btn").click(function() {
			$(".b-section-menu").toggle();
		})

	});

	/* ---------------------------------------------
	 Scripts scroll
	 --------------------------------------------- */
	$(window).scroll(function () {
		if ($(window).scrollTop() == 0) {
			$('.b-scroll__top').stop(false, true).fadeOut(600);
		} else {
			$('.b-scroll__top').stop(false, true).fadeIn(600);
		}

		/* Main menu on top */
		var h = $(window).scrollTop();
		var max_h = $('.b-header__top').height();
		var width = $(window).innerWidth();
		if (width > 767.98) {
			if (h > max_h) {
				// fix top menu
				$('.b-header__bottom').addClass('b-nav-ontop');
				$('.b-sidebar').css('margin-top', '76px');

			} else {
				$('.b-header__bottom').removeClass('b-nav-ontop');
				$('.b-sidebar').css('margin-top', '183px');
			}
		}
	});


})(jQuery); 