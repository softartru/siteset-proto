<?php

/**
 * Вход в crm
 *
 */

require('partials/head.php');

?>
<body class="h-full bg-gray-200 text-gray-900 leading-normal flex flex-col">
	<?php

	require('partials/header.php');

	?>

	<main class="w-full max-w-7xl mx-auto px-2 lg:px-4">
		<ul class="flex items-center text-sm leading-5 my-3">
			<li>
				<a href="/" class="text-gray-800 hover:text-gray-600 focus:outline-none underline">Управление</a>
			</li>
			<li class="text-gray-600 mx-2"> / </li>
			<li class="text-gray-600">Личные данные</li>
		</ul>

		<h1 class="text-2xl font-semibold leading-5 border-b border-gray-400 pb-3 mb-4">Личные данные</h1>

		<div class="bg-white border border-gray-300 mb-3">
    			<form class="w-full max-w-screen-md text-sm my-10 mx-auto">
    				<div class="md:flex md:items-center mb-3">
					<div class="w-full md:w-1/3"></div>
					<div class="w-full md:w-2/3 font-medium text-base">Персональные данные</div>
				</div>
    				<div class="md:flex md:items-center mb-4">
    					<label for="name" class="block w-full md:w-1/3 text-gray-700 font-medium md:text-right mb-1 lg:mb-0 pr-4 leading-5">Имя</label>
    					<div class="w-full md:w-2/3">
    						<input class="w-full bg-white focus:outline-none border border-gray-400 focus:border-gray-500 py-1 px-3 appearance-none" type="text" id="name" placeholder="Иван">
    					</div>
    				</div>
    				<div class="md:flex md:items-center mb-4">
    					<label for="surname" class="block w-full md:w-1/3 text-gray-700 font-medium md:text-right mb-1 lg:mb-0 pr-4 leading-5">Фамилия</label>
    					<div class="w-full md:w-2/3">
    						<input class="w-full bg-white focus:outline-none border border-gray-400 focus:border-gray-500 py-1 px-3 appearance-none" type="text" id="surname" placeholder="Иванов">
    					</div>
    				</div>
    				<div class="md:flex md:items-center mb-4">
    					<label for="birthdate" class="block w-full md:w-1/3 text-gray-700 font-medium md:text-right mb-1 lg:mb-0 pr-4 leading-5">Дата рождения</label>
    					<div class="w-1/3">
    						<input class="w-full bg-white focus:outline-none border border-gray-400 focus:border-gray-500 py-1 px-3 appearance-none" type="text" id="birthdate" placeholder="01.01.2000">
    					</div>
    				</div>

    				<div class="md:flex md:items-center mb-3 mt-4">
					<div class="w-full md:w-1/3"></div>
					<div class="w-full md:w-2/3 font-medium text-base">Учетные данные</div>
				</div>
    				<div class="md:flex md:items-center mb-4">
    					<label for="login" class="block w-full md:w-1/3 text-gray-700 font-medium md:text-right mb-1 lg:mb-0 pr-4 leading-5">Логин</label>
    					<div class="w-full md:w-2/3">
    						<input class="w-full bg-white focus:outline-none border border-gray-400 focus:border-gray-500 py-1 px-3 appearance-none" type="text" id="login" placeholder="Ivan">
    					</div>
    				</div>

    				<div class="md:flex md:items-center mb-3 mt-4">
					<div class="w-full md:w-1/3"></div>
					<div class="w-full md:w-2/3 font-medium text-base">Контактная информация</div>
				</div>
    				<div class="md:flex md:items-center mb-4">
    					<label for="phone" class="block w-full md:w-1/3 text-gray-700 font-medium md:text-right mb-1 lg:mb-0 pr-4 leading-5">Телефон</label>
    					<div class="w-full md:w-2/3">
    						<input class="w-full bg-white focus:outline-none border border-gray-400 focus:border-gray-500 py-1 px-3 appearance-none" type="text" id="phone" placeholder="+7 (123) 456-78-90">
    					</div>
    				</div>
    				<div class="md:flex md:items-center mb-4">
    					<label for="mail" class="block w-full md:w-1/3 text-gray-700 font-medium md:text-right mb-1 lg:mb-0 pr-4 leading-5">E-mail</label>
    					<div class="w-full md:w-2/3">
    						<input class="w-full bg-white focus:outline-none border border-gray-400 focus:border-gray-500 py-1 px-3 appearance-none" type="email" id="mail" placeholder="ivan@mail.ru">
    					</div>
    				</div>
    				<div class="md:flex md:items-center mb-4">
					<div class="w-full md:w-1/3"></div>
					<div class="w-full md:w-2/3">
        					<button type="submit" class="inline-block rounded px-3 py-2 text-sm leading-4 bg-blue-600 hover:bg-blue-700 focus:outline-none text-white cursor-pointer">Сохранить</button>
					</div>
				</div>
    			</form>
		</div>
	</main>

	<?php

	require('partials/footer.php');

	?>
</body></html>