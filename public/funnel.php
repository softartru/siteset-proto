<?php

/**
 * Вход в crm
 *
 */

require('partials/head.php');

?>
<script>
window.onload = function () {

var chart = new CanvasJS.Chart("chartContainer", {
	animationEnabled: true,
	theme: "light2", //"light1", "dark1", "dark2"
	//title:{
		//text: "Sales Analysis - June 2016"
	//},
	data: [{
		type: "funnel",
		indexLabelPlacement: "inside",
		indexLabelFontColor: "white",
		toolTipContent: "<b>{label}</b>: {y} <b>({percentage}%)</b>",
		indexLabel: "{label} ({percentage}%)",
		dataPoints: [
			{ y: 1400, label: "Leads" },
			{ y: 1212, label: "Initial Communication" },
			{ y: 1080, label: "Customer Evaluation" },
			{ y: 665,  label: "Negotiation" },
			{ y: 578, label: "Order Received" },
			{ y: 549, label: "Payment" }
		]
	}]
});
calculatePercentage();
chart.render();

function calculatePercentage() {
	var dataPoint = chart.options.data[0].dataPoints;
	var total = dataPoint[0].y;
	for(var i = 0; i < dataPoint.length; i++) {
		if(i == 0) {
			chart.options.data[0].dataPoints[i].percentage = 100;
		} else {
			chart.options.data[0].dataPoints[i].percentage = ((dataPoint[i].y / total) * 100).toFixed(2);
		}
	}
}

}
</script>
<body class="h-full bg-gray-200 text-gray-900 leading-normal flex flex-col">
	<?php

	require('partials/header.php');

	?>

	<main class="w-full max-w-7xl mx-auto px-2 lg:px-4">
		<ul class="flex items-center text-sm leading-5 my-3">
			<li>
				<a href="/" class="text-gray-800 hover:text-gray-600 focus:outline-none underline">Управление</a>
			</li>
			<li class="text-gray-600 mx-2"> / </li>
			<li class="text-gray-600">Воронка продаж</li>
		</ul>

		<h1 class="text-2xl font-semibold leading-5 border-b border-gray-400 pb-3 mb-4">Воронка продаж</h1>

		<div class="bg-white border border-gray-300 mb-3 px-4 py-6">
			<div class="w-full max-w-screen-md mx-auto flex flex-col">
				<div class="flex justify-center">
         				<div class="flex items-center mx-6">
         					<input id="rd1" name="rd" type="radio" checked> 
         					<label for="rd1" class="ml-2">В этом месяце</label>
         				</div>
         				<div class="flex items-center mx-6">
         					<input id="rd2" name="rd" type="radio"> 
         					<label for="rd2" class="ml-2">В этом году</label>
         				</div>
         				<div class="flex items-center mx-6">
         					<input id="rd3" name="rd" type="radio"> 
         					<label for="rd3" class="ml-2">Всего</label>
         				</div>
				</div>

				<div id="chartContainer" style="height: 400px; width: 100%;"></div>

				<div class="w-full md:w-1/2 mx-auto mt-4 mb-4 relative">
					<select class="block w-full bg-white text-gray-700 focus:outline-none border border-gray-400 focus:border-gray-500 py-1 px-3 appearance-none">
						<option>Все направления</option>
						<option>Первое направление</option>
					</select>
					<div class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-700">
						<svg class="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z"/></svg>
					</div>
				</div>
			</div>
		</div>
	</main>

	<?php

	require('partials/footer.php');

	?>
	<script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>

</body></html>