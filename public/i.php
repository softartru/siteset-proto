﻿<?php

require('partials/head.php');

?>
<body>
	<div class="container mx-auto">
		<h1 class="text-2xl font-semibold pt-12 pb-4">CRM</h1>

		<ul>
			<li><a href="index.php" class="underline hover:no-underline" target="_blank">Вход в CRM</a></li>
			<li><a href="orders.php" class="underline hover:no-underline" target="_blank">Сделки</a></li>
			<li><a href="personal-data.php" class="underline hover:no-underline" target="_blank">Личные данные</a></li>
			<li><a href="funnel.php" class="underline hover:no-underline" target="_blank">Воронка продаж</a></li>
		</ul>
	</div>
</body></html>