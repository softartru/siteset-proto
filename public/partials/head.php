<?php

require '../src/helpers/app.php';

?><!DOCTYPE html>
<html lang="ru" class="h-full">
<head>
	<meta charset="utf-8">
	<title>CRM</title>
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	
	<link rel="stylesheet" href="<?php asset('css/app.css') ?>">
	<script src="<?php asset('js/app.js') ?>"></script>
</head>
