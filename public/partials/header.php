<header class="bg-white">
	<div class="max-w-7xl mx-auto px-2 lg:px-4">
		<div class="relative flex items-center justify-between">
			<div class="relative flex items-center lg:hidden">
				<!-- Mobile menu button-->
				<button class="inline-flex items-center justify-center p-1 text-white bg-blue-700 hover:bg-gray-600 focus:outline-none focus:bg-gray-600 focus:text-white transition duration-150 ease-in-out">
					<!-- Icon when menu is closed. -->
					<!-- Menu open: "hidden", Menu closed: "block" -->
					<svg class="block h-6 w-6" stroke="currentColor" fill="none" viewBox="0 0 24 24">
						<path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4 6h16M4 12h16M4 18h16" />
					</svg>
					<!-- Icon when menu is open. -->
					<!-- Menu open: "block", Menu closed: "hidden" -->
					<svg class="hidden h-6 w-6" stroke="currentColor" fill="none" viewBox="0 0 24 24">
						<path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M6 18L18 6M6 6l12 12" />
					</svg>
				</button>
			</div>

			<div class="flex-1 flex items-center justify-center lg:items-stretch lg:justify-start">
				<div class="flex-shrink-0 pt-1 pb-1 lg:pb-0">
					<img class="block h-8 w-auto" src="images/logo.png" alt="">
				</div>
				<div class="hidden lg:block lg:ml-6">
					<nav class="flex">
						<a href="#" class="px-3 py-2 text-sm font-medium leading-5 text-gray-900 bg-gray-300 focus:outline-none transition duration-150 ease-in-out">Документы</a>
						<a href="#" class="ml-2 px-3 py-2 text-sm font-medium leading-5 text-gray-700 hover:text-gray-900 bg-gray-100 focus:outline-none transition duration-150 ease-in-out">Справочники</a>
						<a href="#" class="ml-2 px-3 py-2 text-sm font-medium leading-5 text-gray-700 hover:text-gray-900 bg-gray-100 focus:outline-none transition duration-150 ease-in-out">Аналитика</a>
					</nav>
				</div>
			</div>

			<div class="flex items-center text-sm">
				<a href="#" title="Разработчик" class="mr-4 text-gray-700 hover:text-gray-900">
					<i class="fa fa-user-o" aria-hidden="true"></i><span class="hidden xl:inline-block xl:ml-1"><b>Разработчик</b> &nbsp; Администратор</span>
				</a>
				<a href="/logout" class="text-gray-700 hover:text-gray-900" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
					<i class="fa fa-sign-out" aria-hidden="true"></i><span class="hidden xl:inline-block xl:ml-1">Выход</span>
				</a>
			</div>
		</div>
	</div>

	<div class="hidden lg:block">
		<nav class="max-w-7xl mx-auto flex pl-1 pr-4 nav-menu">
			<a href="#" class="relative px-3 pt-1 pb-2 text-sm font-medium leading-5 text-gray-700 hover:text-gray-900 active">Входящие</a>
			<!--<a href="#" class="relative ml-2 px-3 pt-1 pb-2 text-sm font-medium leading-5 text-gray-700 hover:text-gray-900">...</a>-->
		</nav>

		<div class="bg-blue-800">
			<nav class="max-w-7xl mx-auto flex pl-1 pr-4 nav-submenu">
				<a href="#" class="relative mx-3 pt-1 pb-2 text-sm leading-5 text-gray-300 hover:text-white active">Сделки</a>
				<a href="#" class="relative ml-2 mx-3 py-1 text-sm leading-5 text-gray-300 hover:text-white">Звонки</a>
			</nav>
		</div>
	</div>
</header>