<div class="modal fixed left-0 w-full h-full z-50 overflow-x-hidden hidden fade win-fade win-navi" id="win-nav">
	<div class="fixed inset-0 transition-opacity b-backdrop">
		<div class="absolute inset-0 bg-black opacity-50"></div>
	</div>
	<div class="win-nav w-11/12">
		<div class="bg-white leading-5">
			<div class="modal-body">
				<div id="jquery-accordion-menu" class="jquery-accordion-menu white">
					<ul id="menu-list">
						<li class="active"><a href="index.php">Главная</a></li>
						<li><a href="shop.php">Каталог</a>
							<ul class="submenu">
								<li>
									<a href="shop-group.php">Для неё</a>
									<ul class="submenu">
										<li><a href="#">Название категории</a></li>
										<li><a href="#">Название категории</a></li>
										<li><a href="#">Название категории</a></li>
									</ul>
								</li>
								<li>
									<a href="#">Для него</a>
								</li>
								<li>
									<a href="#">Для пары</a>
								</li>
								<li>
									<a href="#">Одежда и Белье</a>
								</li>
								<li>
									<a href="#">Аксессуары и фетиш</a>
								</li>
								<li>
									<a href="#">Интимная косметика</a>
								</li>
								<li>
									<a href="#">Феромоны</a>
								</li>
								<li>
									<a href="#">Подарочные наборы</a>
								</li>
							</ul>
						</li>
						<li><a href="deals.php">Акции</a></li>
						<li>
							<a href="about.php">О компании</a>
							<ul class="submenu">
								<li><a href="delivery.php">Доставка</a></li>
								<li><a href="payment.php">Оплата</a></li>
								<li><a href="reviews.php">Отзывы о магазине</a></li>
								<li><a href="feedback.php">Обратная связь</a></li>
							</ul>
						</li>
						<li><a href="news.php">Новости</a></li>
						<li><a href="articles.php">Статьи</a></li>
						<li><a href="blog.php">Блог</a></li>
						<li><a href="contacts.php">Контакты</a></li>

					</ul>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- личный кабинет -->
<div class="fixed top-0 left-0 w-full h-full z-50 overflow-hidden hidden win-m-profile">
	<div class="fixed inset-0 transition-opacity">
		<div class="absolute inset-0 bg-black opacity-50"></div>
	</div>
	<div class="modal-win-profile w-11/12 bg-white relative">
		<button type="button" onclick="win.toggle('m-profile');" class="position: absolute right-0 inline-flex items-center justify-center p-1 text-gray-500 hover:text-gray-600 focus:outline-none transition duration-150 ease-in-out">
			<svg class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
				<path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M6 18L18 6M6 6l12 12"></path>
			</svg>
		</button>
		<h4 class="text-lg text-center pt-4 pb-2 font-medium leading-tight">Константин<br>Личный кабинет</h4>
		<div class="text-sm font-medium">
			<a href="account.php" class="block px-4 py-2 hover:bg-brown-200 focus:outline-none">Сводная информация</a>
			<a href="account-profile.php" class="block px-4 py-2 hover:bg-brown-200 focus:outline-none">Изменение профиля</a>
			<a href="account-password.php" class="bg-brown-200 block px-4 py-2 hover:bg-brown-200 focus:outline-none">Изменение пароля</a>
			<!-- активное меню: bg-brown-200-->
			<a href="account-notifications.php" class="block px-4 py-2 hover:bg-brown-200 focus:outline-none">Настройка уведомлений</a>
			<a href="account-history.php" class="block px-4 py-2 hover:bg-brown-200 focus:outline-none">История заказов</a>
			<a href="#" class="block px-4 py-2 hover:bg-brown-200 focus:outline-none">Выход</a>
		</div>
	</div>
</div>

<!-- вход -->
<div class="fixed top-0 left-0 w-full h-full z-50 overflow-hidden hidden win-m-logon">
	<div class="fixed inset-0 transition-opacity">
		<div class="absolute inset-0 bg-black opacity-50"></div>
	</div>
	<div class="w-11/12 bg-white relative">
		<button type="button" onclick="win.toggle('m-logon');" class="position: absolute right-0 inline-flex items-center justify-center p-1 text-gray-500 hover:text-gray-600 focus:outline-none transition duration-150 ease-in-out">
			<svg class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
				<path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M6 18L18 6M6 6l12 12"></path>
			</svg>
		</button>
		<nav class="b-win-logon-nav flex justify-center">
			<a class="border-b-2 border-brown-600 px-4 py-2 mx-1 font-medium" href="#logon">Вход</a>
			<a class="border-b-2 border-gray-500 px-4 py-2 mx-1 font-medium" href="#registration">Регистрация</a>
		</nav>
		<div class="tab-content text-sm p-4">
			<div class="tab-pane show active" id="logon">
				<form id="formProfile" method="post" action="/logon/?referer=/profile/">
					<input type="hidden" name="event" value="logon">
					<input type="hidden" name="referer" value="/profile/">

					<div class="mb-3">
						<label class="block mb-1" for="i-login">Логин:</label>
						<input id="i-login" name="login" type="text" class="rounded appearance-none w-full py-1 px-2 bg-white border border-gray-400 leading-tight focus:outline-none focus:border-gray-500">
					</div>
					<div class="mb-3">
						<label class="block mb-1" for="i-nputpassword">Пароль:</label>
						<input id="i-nputpassword" name="password" type="password" class="rounded appearance-none w-full py-1 px-2 bg-white border border-gray-400 leading-tight focus:outline-none focus:border-gray-500">
					</div>
					<div class="flex items-center">
						<button class="bg-brown-600 leading-tight hover:bg-brown-700 text-white py-2 px-4 text-xs text-center font-medium rounded-full focus:outline-none" type="submit">Войти</button>
						<a href="/remind-password/" class="underline ml-6 hover:no-underline">Забыли пароль?</a>
					</div>
				</form>
			</div>

			<div class="tab-pane hidden" id="registration">
				<form action="#" method="post">
					<div class="mb-3">
						<label class="block mb-1" for="r-email">E-mail <span class="text-red-600">*</span></label>
						<input type="text" name="r-email" class="appearance-none w-full py-1 px-2 bg-white border border-gray-400 leading-tight focus:outline-none focus:border-gray-500" placeholder="E-mail">
					</div>
					<div class="mb-3">
						<label class="block mb-1" for="r-password">Пароль <span class="text-red-600">*</span></label>
						<input type="password" name="r-password" class="rounded appearance-none w-full py-1 px-2 bg-white border border-gray-400 leading-tight focus:outline-none focus:border-gray-500 form-control-sm" placeholder="Пароль">
					</div>
					<div class="mb-3">
						<label class="block mb-1" for="r-passconf">Пароль ещё раз <span class="text-red-600">*</span></label>
						<input type="password" name="r-passconf" class="rounded appearance-none w-full py-1 px-2 bg-white border border-gray-400 leading-tight focus:outline-none focus:border-gray-500" placeholder="Пароль">
					</div>
					<div class="mb-3">
						<label class="flex items-center">
							<input class="form-checkbox transition duration-150 ease-in-out mr-2" type="checkbox">
							<span class="text-sm leading-5">Я согласен на обработку <a class="underline hover:no-underline" href="#">персональных данных</a></span>
						</label>
						<div class="b-policy-error text-sm leading-tight mt-2">Вы не дали согласие на обработку персональных данных</div>
					</div>
					<div class="text-center">
						<button class="bg-brown-600 leading-tight hover:bg-brown-700 text-white py-2 px-4 text-xs text-center font-medium rounded-full focus:outline-none" type="submit">Зарегистрироваться</button>
						<div class="mt-2"><a class="underline hover:no-underline" href="#">Забыли пароль?</a></div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

<!-- корзина -->
<div class="fixed top-0 left-0 w-full h-full z-50 overflow-hidden hidden win-m-cart">
	<div class="fixed inset-0 transition-opacity">
		<div class="absolute inset-0 bg-black opacity-50"></div>
	</div>
	<div class="modal-win-cart w-11/12 float-right bg-white relative">
		<button type="button" onclick="win.toggle('m-cart');" class="position: absolute right-0 inline-flex items-center justify-center p-1 text-gray-500 hover:text-gray-600 focus:outline-none transition duration-150 ease-in-out">
			<svg class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
				<path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M6 18L18 6M6 6l12 12"></path>
			</svg>
		</button>
		<h4 class="text-lg text-center pt-4 font-semibold">Ваша корзина</h4>

		<div class="relative overflow-x-hidden">
			<div class="relative leading-tight py-2 pl-3 pr-6 border-b">
				<div class="absolute right-0 mr-1 p-1 cursor-pointer text-brown-600 hover:text-brown-800" title="Удалить товар из корзины">
					<svg class="stroke-current w-5 h-5" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
						<path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 7l-.867 12.142A2 2 0 0116.138 21H7.862a2 2 0 01-1.995-1.858L5 7m5 4v6m4-6v6m1-10V4a1 1 0 00-1-1h-4a1 1 0 00-1 1v3M4 7h16" />
					</svg>
				</div>
				<div class="text-sm">
					<a href="shop-detail.php" class="underline hover:no-underline">Бэби-долл с рюшами Akena</a>
					<div class="text-brown-600">Код: 01-1</div>
				</div>
				<div class="flex items-center mt-2 text-sm">
					<span class="inline-flex bg-brown-200 rounded-full overflow-hidden">
						<span class="cursor-pointer p-1 pl-2">
							<svg class="stroke-current w-4 h-4" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
								<path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M18 12H6" />
							</svg>
						</span>
						<input value="1" type="text" class="bg-brown-200 font-medium focus:outline-none w-6 text-center">
						<span class="cursor-pointer p-1 pr-2">
							<svg class="stroke-current w-4 h-4" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
								<path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 6v6m0 0v6m0-6h6m-6 0H6" />
							</svg>
						</span>
					</span>
					<span class="ml-2">&times; 1150&nbsp;руб. = <b class="whitespace-no-wrap">1150</b>&nbsp;руб.</span>
				</div>
			</div>
			<div class="relative leading-tight py-2 pl-3 pr-6 border-b">
				<div class="absolute right-0 mr-1 p-1 cursor-pointer text-brown-600 hover:text-brown-800" title="Удалить товар из корзины">
					<svg class="stroke-current w-5 h-5" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
						<path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 7l-.867 12.142A2 2 0 0116.138 21H7.862a2 2 0 01-1.995-1.858L5 7m5 4v6m4-6v6m1-10V4a1 1 0 00-1-1h-4a1 1 0 00-1 1v3M4 7h16" />
					</svg>
				</div>
				<div class="text-sm">
					<a href="shop-detail.php" class="underline hover:no-underline">Серебристая вибропуля Erowoman-Eroman</a>
					<div class="text-brown-600">Код: 10</div>
				</div>
				<div class="flex items-center mt-2 text-sm">
					<span class="inline-flex bg-brown-200 rounded-full overflow-hidden">
						<span class="cursor-pointer p-1 pl-2">
							<svg class="stroke-current w-4 h-4" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
								<path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M18 12H6" />
							</svg>
						</span>
						<input value="1" type="text" class="bg-brown-200 font-medium focus:outline-none w-6 text-center">
						<span class="cursor-pointer p-1 pr-2">
							<svg class="stroke-current w-4 h-4" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
								<path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 6v6m0 0v6m0-6h6m-6 0H6" />
							</svg>
						</span>
					</span>
					<span class="ml-2">&times; 338&nbsp;руб. = <b class="whitespace-no-wrap">338</b>&nbsp;руб.</span>
				</div>
			</div>
		</div>
		<div class="flex items-center justify-between bg-white py-2 px-3">
			<div>Итого: <b>1488</b> руб.</div>
			<div class="cursor-pointer text-brown-600 border border-brown-600 rounded px-2 py-1 hover:bg-brown-200" title="Удалить товар из корзины">
				<svg class="stroke-current w-5 h-5" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
					<path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 7l-.867 12.142A2 2 0 0116.138 21H7.862a2 2 0 01-1.995-1.858L5 7m5 4v6m4-6v6m1-10V4a1 1 0 00-1-1h-4a1 1 0 00-1 1v3M4 7h16" />
				</svg>
			</div>
		</div>
		<div class="px-3 pb-2">
			<a class="inline-block leading-tight bg-brown-600 hover:bg-brown-700 text-white py-2 px-4 text-xs text-center font-medium rounded-full focus:outline-none" href="cart-order.php">Оформить заказ</a>
		</div>
	</div>
</div>