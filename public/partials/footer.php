<footer class="mt-auto pb-3">
	<div class="max-w-7xl mx-auto px-2 lg:px-4 lg:flex lg:items-center lg:justify-between text-center lg:text-left">
		<div class="text-xs text-gray-600">SiteSet CMS • Версия 1.0.0 <span class="hidden lg:inline">•</span> <span class="block lg:inline">&copy&nbsp;2018–2020 <a href="http://softart.ru" class="text-gray-600 hover:text-gray-800 underline">Интернет-компания СофтАрт</a></div>
		<div class="text-xs text-gray-600">Powered by <a href="https://laravel.com" class="text-gray-600 hover:text-gray-800 underline">Laravel Framework</a>&nbsp;v5.6.39</div>
	</div>
</footer>
