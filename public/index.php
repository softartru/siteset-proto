<?php

/**
 * Вход в crm
 *
 */

require('partials/head.php');

?>

<body class="h-full bg-white bg-fixed bg-cover bg-no-repeat bg-center text-gray-900" style="background-image: url(images/body-bg.jpg);">
	<div class="container mx-auto px-4">
		<div class="md:flex">
			<div class="relative max-w-xs w-full md:flex-auto mt-8 md:mt-12">
				<div class="absolute inset-0 h-full w-full rounded-lg flex items-end ml-3 md:ml-8 mt-16 bg-gray-800 bg-opacity-50 z-0">
					<div class="pl-8 pb-6"><a href="/register" class="text-white hover:text-blue-200 text-lg underline">Регистрация</a></div>
				</div>

				<div class="relative bg-gray-100 rounded-lg shadow-md p-4 lg:p-8 flex-shrink-0 lg:flex-shrink z-10">
					<img src="images/logo-b.png" alt="CRM" class="mx-auto mb-8">
					<form method="POST" action="/login">
						<div class="mb-4">
							<input type="text" name="email" class="w-full appearance-none bg-white border border-gray-400 focus:border-blue-500 text-gray-900 py-2 px-4 focus:outline-none" placeholder="E-Mail">
						</div>
						<div class="mb-6">
							<input type="password" name="password" class="w-full appearance-none bg-white border border-gray-400 focus:border-blue-500 text-gray-900 py-2 px-4 focus:outline-none" placeholder="Пароль">
						</div>
						<button type="submit" class="block bg-blue-500 hover:bg-blue-700 text-white font-medium uppercase py-2 px-6 mx-auto">Войти</button>
						<div class="text-center leading-tight mt-4"><a href="/password/reset" class="text-sm underline hover:text-gray-700">Забыли<br>логин или пароль?</a></div>
					</form>
				</div>
			</div>
			<div class="md:flex-auto">
				<div class="mt-16 p-4 md:p-8">
					<p class="text-base lg:text-xl md:ml-12">Разработка, размещение, поддержка и продвижение интернет-проектов на основе Laravel и Vue.js</p>
				</div>
			</div>
		</div>
	</div>
</body>

</html>