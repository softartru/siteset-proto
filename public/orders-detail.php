<?php

/**
 * 
 *
 */

require('partials/head.php');

?>

<body class="h-full bg-gray-200 text-gray-900 leading-normal flex flex-col">
	<?php

	require('partials/header.php');

	?>

	<main class="w-full max-w-7xl mx-auto px-2 lg:px-4">
		<ul class="flex items-center text-sm leading-5 my-3">
			<li>
				<a href="/" class="text-gray-800 hover:text-gray-600 focus:outline-none underline">Управление</a>
			</li>
			<li class="text-gray-600 mx-2"> / </li>
			<li>
				<a href="orders.php" class="text-gray-800 hover:text-gray-600 focus:outline-none underline">Сделки</a>
			</li>
			<li class="text-gray-600 mx-2"> / </li>
			<li class="text-gray-600">Заказ #2080 от 2020-11-17 11:07:00</li>
		</ul>

		<h1 class="text-2xl font-semibold leading-5 border-b border-gray-400 pb-3 mb-4">Заказ #2080 от 2020-11-17 11:07:00</h1>

		<div class="flex flex-wrap -mx-2">
			<div class="w-full lg:w-1/3 xl:w-1/4 px-2">
				<div class="bg-white border border-gray-300 mb-3">
					<div class="p-3 border-b border-gray-300">
						<div class="flex justify-between">
							<a class="inline-block rounded px-3 py-2 text-sm leading-4 bg-gray-300 hover:bg-gray-400" href="#" title="Назад">
								<i class="fa fa-arrow-left"></i>
							</a>
							<a class="ml-auto inline-block rounded px-3 py-2 text-sm leading-4 bg-gray-300 hover:bg-gray-400" href="#">
								<i class="fa fa-print" aria-hidden="true"></i> Печатать
							</a>
							<a class="ml-1 inline-block rounded px-3 py-2 text-sm leading-4 bg-blue-600 hover:bg-blue-700 text-white" href="#" title="Изменить">
								<i class="fa fa-pencil"></i> Изменить
							</a>
						</div>
					</div>
					<div class="p-3">
						<div class="flex flex-wrap justify-center text-sm leading-4">
							<div class="w-1/2 py-1 px-2"></div>
							<div class="w-1/2 py-1 px-2"><b>Свойства</b></div>
							<div class="w-1/2 py-1 px-2 text-right">Опубликовано</div>
							<div class="w-1/2 py-1 px-2">17.11.2020 11:07</div>
							<div class="w-1/2 py-1 px-2 text-right">Сумма</div>
							<div class="w-1/2 py-1 px-2">2 190.00 р.</div>
							<div class="w-1/2 py-1 px-2 text-right">Статус заказа</div>
							<div class="w-1/2 py-1 px-2 bg-yellow-300"><a class="underline hover:no-underline" href="#">В работе</a></div>
							<div class="w-1/2 py-1 px-2 text-right">Метод доставки</div>
							<div class="w-1/2 py-1 px-2"><a class="underline text-blue-600 hover:no-underline" href="#">Курьером</a></div>
							<div class="w-1/2 py-1 px-2 text-right">Пользователь</div>
							<div class="w-1/2 py-1 px-2">Нет</div>
							<div class="w-1/2 py-1 px-2 text-right">Заказчик</div>
							<div class="w-1/2 py-1 px-2">Алексей</div>
							<div class="w-1/2 py-1 px-2 text-right">Телефон заказчика</div>
							<div class="w-1/2 py-1 px-2">+7 (123) 456-78-90</div>
							<div class="w-1/2 py-1 px-2 text-right">E-mail заказчика</div>
							<div class="w-1/2 py-1 px-2">info@mail.ru</div>
							<div class="w-1/2 py-1 px-2 text-right">Адрес доставки</div>
							<div class="w-1/2 py-1 px-2">Нижний Новгород, ул.Мира д.1 кв.4</div>
							<div class="w-1/2 py-1 px-2 text-right">Комментарий</div>
							<div class="w-1/2 py-1 px-2">нет</div>
						</div>
					</div>
				</div>
			</div>

			<div class="w-full lg:w-2/3 xl:w-3/4 px-2">
				<div class="bg-white border border-gray-300 mb-3">
					<div id="tabs">
						<ul>
							<li><a href="#tabs-1">Продажи</a></li>
							<li><a href="#tabs-2">Продажи</a></li>
							<li><a href="#tabs-3">Продажи</a></li>
						</ul>
					</div>

					<div id="tabs-1">
						<div class="p-3 border-b border-gray-300">
							<div class="inline-block rounded px-3 py-2 text-sm leading-4 bg-blue-600 hover:bg-blue-700 text-white cursor-pointer"><i class="fa fa-plus" aria-hidden="true"></i> Создать</div>
						</div>

						<div class="p-3">
							<div class="overflow-x-auto pb-2 -mb-2">
								<table class="min-w-full text-sm">
									<thead>
										<tr>
											<th class="p-2 border-b border-gray-300 bg-gray-100" width="30"></th>
											<th class="p-2 border-b border-gray-300 bg-gray-100 text-left align-top leading-4 font-medium text-gray-700"><a href="#" class="underline hover:text-gray-900">Код</a></th>
											<th class="p-2 border-b border-gray-300 bg-gray-100 text-left align-top leading-4 font-medium text-gray-700"><a href="#" class="underline hover:text-gray-900">Наименование товара</a></th>
											<th class="p-2 border-b border-gray-300 bg-gray-100 text-right align-top leading-4 font-medium text-gray-700"><a href="#" class="underline hover:text-gray-900">Цена без скидки</a></th>
											<th class="p-2 border-b border-gray-300 bg-gray-100 text-right align-top leading-4 font-medium text-gray-700"><a href="#" class="underline hover:text-gray-900">Скидка</a></th>
											<th class="p-2 border-b border-gray-300 bg-gray-100 text-right align-top leading-4 font-medium text-gray-700"><a href="#" class="underline hover:text-gray-900">Цена со скидкой</a></th>
											<th class="p-2 border-b border-gray-300 bg-gray-100 text-right align-top leading-4 font-medium text-gray-700"><a href="#" class="underline hover:text-gray-900">Кол-во</a></th>
											<th class="p-2 border-b border-gray-300 bg-gray-100 text-right align-top leading-4 font-medium text-gray-700"><a href="#" class="underline hover:text-gray-900">Сумма</a></th>
											<th class="p-2 border-b border-gray-300 bg-gray-100"></th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td class="px-2 pl-0 py-1 border-b border-gray-300 leading-4">
												<a href="#" class="inline-block rounded px-2 py-1 bg-gray-300 hover:bg-gray-400" title="Показать"><i class="fa fa-eye"></i></a>
											</td>
											<td class="px-2 py-1 border-b border-gray-300 leading-4">30001917</td>
											<td class="px-2 py-1 border-b border-gray-300 leading-4">Батарея салютов "Жар-птица"</td>
											<td class="px-2 py-1 border-b border-gray-300 leading-4 text-right whitespace-no-wrap">1 560.00 р.</td>
											<td class="px-2 py-1 border-b border-gray-300 leading-4 text-right whitespace-no-wrap">156.00 р.</td>
											<td class="px-2 py-1 border-b border-gray-300 leading-4 text-right whitespace-no-wrap">1 404.00 р.</td>
											<td class="px-2 py-1 border-b border-gray-300 leading-4 text-right">1</td>
											<td class="px-2 py-1 border-b border-gray-300 leading-4 text-right whitespace-no-wrap">1 404.00 р.</td>
											<td class="px-2 pr-0 py-1 border-b border-gray-300 text-right whitespace-no-wrap leading-4">
												<a onclick="" href="javascript: void(0);" class="inline-block rounded px-2 py-1 bg-blue-600 hover:bg-blue-700 text-white" data-toggle="tooltip" title="Изменить">
													<i class="fa fa-pencil"></i> Изменить
												</a>
												<button onclick="" class="inline-block rounded px-2 py-1 bg-red-600 hover:bg-red-700 text-white focus:outline-none" data-toggle="tooltip" title="Удалить">
													<i class="fa fa-trash"></i>
												</button>
											</td>
										</tr>
										<tr>
											<td class="px-2 pl-0 py-1 border-b border-gray-300 leading-4">
												<a href="#" class="inline-block rounded px-2 py-1 bg-gray-300 hover:bg-gray-400" title="Показать"><i class="fa fa-eye"></i></a>
											</td>
											<td class="px-2 py-1 border-b border-gray-300 leading-4">30001917</td>
											<td class="px-2 py-1 border-b border-gray-300 leading-4">Батарея салютов "Жар-птица"</td>
											<td class="px-2 py-1 border-b border-gray-300 leading-4 text-right whitespace-no-wrap">1 560.00 р.</td>
											<td class="px-2 py-1 border-b border-gray-300 leading-4 text-right whitespace-no-wrap">156.00 р.</td>
											<td class="px-2 py-1 border-b border-gray-300 leading-4 text-right whitespace-no-wrap">1 404.00 р.</td>
											<td class="px-2 py-1 border-b border-gray-300 leading-4 text-right">1</td>
											<td class="px-2 py-1 border-b border-gray-300 leading-4 text-right whitespace-no-wrap">1 404.00 р.</td>
											<td class="px-2 pr-0 py-1 border-b border-gray-300 text-right whitespace-no-wrap leading-4">
												<a onclick="" href="javascript: void(0);" class="inline-block rounded px-2 py-1 bg-blue-600 hover:bg-blue-700 text-white" data-toggle="tooltip" title="Изменить">
													<i class="fa fa-pencil"></i> Изменить
												</a>
												<button onclick="" class="inline-block rounded px-2 py-1 bg-red-600 hover:bg-red-700 text-white focus:outline-none" data-toggle="tooltip" title="Удалить">
													<i class="fa fa-trash"></i>
												</button>
											</td>
										</tr>
										<tr>
											<td class="px-2 pl-0 py-1 border-b border-gray-300 leading-4">
												<a href="#" class="inline-block rounded px-2 py-1 bg-gray-300 hover:bg-gray-400" title="Показать"><i class="fa fa-eye"></i></a>
											</td>
											<td class="px-2 py-1 border-b border-gray-300 leading-4">30001917</td>
											<td class="px-2 py-1 border-b border-gray-300 leading-4">Батарея салютов "Жар-птица"</td>
											<td class="px-2 py-1 border-b border-gray-300 leading-4 text-right whitespace-no-wrap">1 560.00 р.</td>
											<td class="px-2 py-1 border-b border-gray-300 leading-4 text-right whitespace-no-wrap">156.00 р.</td>
											<td class="px-2 py-1 border-b border-gray-300 leading-4 text-right whitespace-no-wrap">1 404.00 р.</td>
											<td class="px-2 py-1 border-b border-gray-300 leading-4 text-right">1</td>
											<td class="px-2 py-1 border-b border-gray-300 leading-4 text-right whitespace-no-wrap">1 404.00 р.</td>
											<td class="px-2 pr-0 py-1 border-b border-gray-300 text-right whitespace-no-wrap leading-4">
												<a onclick="" href="javascript: void(0);" class="inline-block rounded px-2 py-1 bg-blue-600 hover:bg-blue-700 text-white" data-toggle="tooltip" title="Изменить">
													<i class="fa fa-pencil"></i> Изменить
												</a>
												<button onclick="" class="inline-block rounded px-2 py-1 bg-red-600 hover:bg-red-700 text-white focus:outline-none" data-toggle="tooltip" title="Удалить">
													<i class="fa fa-trash"></i>
												</button>
											</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
					<div id="tabs-2">
						ghhhhhhhhhhhhhhhhh
					</div>
				</div>
			</div>
		</div>
	</main>

	<?php

	require('partials/footer.php');

	?>

	<script>
		$( "#tabs" ).tabs();
	</script>
</body>

</html>