<?php

/**
 * 
 *
 */

require('partials/head.php');

?>

<body class="h-full bg-gray-200 text-gray-900 leading-normal flex flex-col">
	<?php

	require('partials/header.php');

	?>

	<main class="w-full max-w-7xl mx-auto px-2 lg:px-4">
		<ul class="flex items-center text-sm leading-5 my-3">
			<li>
				<a href="/" class="text-gray-800 hover:text-gray-600 focus:outline-none underline">Управление</a>
			</li>
			<li class="text-gray-600 mx-2"> / </li>
			<li class="text-gray-600">Сделки</li>
		</ul>

		<h1 class="text-2xl font-semibold leading-5 border-b border-gray-400 pb-3 mb-4">Сделки</h1>

		<div class="bg-white border border-gray-300 mb-3">
			<div class="p-3 pb-0 border-b border-gray-300">
				<div class="flex justify-between">
					<div class="inline-block rounded px-3 py-2 text-sm leading-4 bg-blue-600 hover:bg-blue-700 text-white cursor-pointer"><i class="fa fa-plus" aria-hidden="true"></i> Создать</div>
					<div class="inline-block rounded px-3 py-2 text-sm leading-4 bg-gray-300 hover:bg-gray-400 text-gray-900 cursor-pointer"><i class="fa fa-download" aria-hidden="true"></i> Экспортировать</div>
				</div>
				<div class="text-center mt-2">
					<div data-ss-filter-toggel=".order-filter" class="inline-block rounded-t px-3 py-2 text-sm leading-4 bg-blue-600 hover:bg-blue-700 text-white cursor-pointer" title="Фильтр">Фильтр <i class="fa fa-caret-down" aria-hidden="true"></i></div>
				</div>
			</div>

			<!-- фильтр -->
			<div class="order-filter bg-gray-300 p-3 pt-6">
				<form class="w-full max-w-screen-xl mx-auto text-sm" method="get">
					<div class="flex flex-wrap lg:-mx-4">
						<div class="w-full lg:w-1/2 lg:px-4">
							<div class="md:flex md:items-center mb-4">
								<label for="f_id" class="block w-full md:w-1/3 text-gray-700 font-medium md:text-right mb-1 lg:mb-0 pr-4">Код</label>
								<div class="w-full md:w-2/3">
									<input class="w-full bg-white focus:outline-none border border-gray-400 focus:border-gray-500 py-1 px-3 appearance-none" type="text" id="f_id" name="f_id" value="">
								</div>
							</div>
						</div>

						<div class="w-full lg:w-1/2 lg:px-4">
							<div class="md:flex md:items-center mb-4">
								<label for="f_posted_at" class="block w-full md:w-1/3 text-gray-700 font-medium md:text-right mb-1 lg:mb-0 pr-4">Опубликовано</label>
								<div class="w-full md:w-2/3">
									<div id="f_posted_at" class="flex">
										<input class="flex-1 w-full bg-white focus:outline-none border border-gray-400 focus:border-gray-500 py-1 px-3 appearance-none" type="text" id="f_posted_at0" name="f_posted_at[0]" value="">
										<span>&nbsp;&ndash;&nbsp;</span>
										<input class="flex-1 w-full bg-white focus:outline-none border border-gray-400 focus:border-gray-500 py-1 px-3 appearance-none input-range" type="text" id="f_posted_at1" name="f_posted_at[1]" value="">
									</div>
								</div>
							</div>
						</div>

						<div class="w-full lg:w-1/2 lg:px-4">
							<div class="md:flex md:items-center mb-4">
								<label for="f_delivery_name" class="block w-full md:w-1/3 text-gray-700 font-medium md:text-right mb-1 lg:mb-0 pr-4">Заказчик</label>
								<div class="w-full md:w-2/3">
									<input class="w-full bg-white focus:outline-none border border-gray-400 focus:border-gray-500 py-1 px-3 appearance-none" type="text" id="f_delivery_name" name="f_delivery_name" value="">
								</div>
							</div>
						</div>

						<div class="w-full lg:w-1/2 lg:px-4">
							<div class="md:flex md:items-center mb-4">
								<label for="f_price_amount" class="block w-full md:w-1/3 text-gray-700 font-medium md:text-right mb-1 lg:mb-0 pr-4">Сумма</label>
								<div class="w-full md:w-2/3">
									<div id="f_price_amount" class="flex">
										<input class="flex-1 w-full bg-white focus:outline-none border border-gray-400 focus:border-gray-500 py-1 px-3 appearance-none" type="text" id="f_price_amount0" name="f_price_amount[0]" value="">
										<span>&nbsp;&ndash;&nbsp;</span>
										<input class="flex-1 w-full bg-white focus:outline-none border border-gray-400 focus:border-gray-500 py-1 px-3 appearance-none input-range" type="text" id="f_price_amount1" name="f_price_amount[1]" value="">
									</div>
								</div>
							</div>
						</div>

						<div class="w-full lg:w-1/2 lg:px-4">
							<div class="md:flex md:items-center mb-4">
								<label for="f_delivery_phone" class="block w-full md:w-1/3 text-gray-700 font-medium md:text-right mb-1 lg:mb-0 pr-4">Телефон заказчика</label>
								<div class="w-full md:w-2/3">
									<input data-mask="+7 (000) 000-00-00" class="w-full bg-white focus:outline-none border border-gray-400 focus:border-gray-500 py-1 px-3 appearance-none" type="text" id="f_delivery_phone" name="f_delivery_phone" value="">
								</div>
							</div>
						</div>

						<div class="w-full lg:w-1/2 lg:px-4">
							<div class="md:flex md:items-center mb-4">
								<label for="f_orderstatus_id" class="block w-full md:w-1/3 text-gray-700 font-medium md:text-right mb-1 lg:mb-0 pr-4">Статус заказа</label>
								<div class="w-full md:w-2/3">
									<input type="hidden" name="f_orderstatus_id" value="">

									<div id="f_orderstatus_id" class="flex">
										<input placeholder="Выберите значение" class="flex-1 w-full bg-white focus:outline-none border border-gray-400 focus:border-gray-500 py-1 px-3 appearance-none ss-search-select-result" type="text" value="">
										<div class="bg-gray-200 border border-l-0 border-gray-400 text-gray-600 py-1 px-2 cursor-pointer"><i class="ss-search-deselect fa fa-times"></i></div>
									</div>
								</div>
							</div>
						</div>

						<div class="w-full lg:w-1/2 lg:px-4">
							<div class="md:flex md:items-center mb-4">
								<label for="f_delivery_email" class="block w-full md:w-1/3 text-gray-700 font-medium md:text-right mb-1 lg:mb-0 pr-4">E-mail заказчика</label>
								<div class="w-full md:w-2/3">
									<input class="w-full bg-white focus:outline-none border border-gray-400 focus:border-gray-500 py-1 px-3 appearance-none" type="text" id="f_E-mail заказчика" name="f_delivery_email" value="">
								</div>
							</div>
						</div>

						<div class="w-full lg:w-1/2 lg:px-4">
							<div class="md:flex md:items-center mb-4">
								<label for="f_deliverymethod_id" class="block w-full md:w-1/3 text-gray-700 font-medium md:text-right mb-1 lg:mb-0 pr-4">Метод доставки</label>
								<div class="w-full md:w-2/3">
									<input type="hidden" name="f_deliverymethod_id" value="">

									<div id="f_deliverymethod_id" class="flex">
										<input placeholder="Выберите значение" class="flex-1 w-full bg-white focus:outline-none border border-gray-400 focus:border-gray-500 py-1 px-3 appearance-none ss-search-select-result" type="text" value="">
										<div class="bg-gray-200 border border-l-0 border-gray-400 text-gray-600 py-1 px-2 cursor-pointer"><i class="ss-search-deselect fa fa-times"></i></div>
									</div>
								</div>
							</div>
						</div>

						<div class="w-full lg:w-1/2 lg:px-4">
							<div class="md:flex md:items-center mb-4">
								<label for="f_shipped_at" class="block w-full md:w-1/3 text-gray-700 font-medium leading-4 md:text-right mb-1 lg:mb-0 pr-4">Дата и время доставки</label>
								<div class="w-full md:w-2/3">
									<div id="f_shipped_at" class="flex">
										<input class="flex-1 w-full bg-white focus:outline-none border border-gray-400 focus:border-gray-500 py-1 px-3 appearance-none" type="text" id="f_shipped_at0" name="f_shipped_at[0]" value="">
										<span>&nbsp;&ndash;&nbsp;</span>
										<input class="flex-1 w-full bg-white focus:outline-none border border-gray-400 focus:border-gray-500 py-1 px-3 appearance-none input-range" type="text" id="f_shipped_at1" name="f_shipped_at[1]" value="">
									</div>
								</div>
							</div>
						</div>

						<div class="w-full lg:w-1/2 lg:px-4">
							<div class="md:flex md:items-center mb-4">
								<label for="f_paymentmethod_id" class="block w-full md:w-1/3 text-gray-700 font-medium md:text-right mb-1 lg:mb-0 pr-4">Метод оплаты</label>
								<div class="w-full md:w-2/3">
									<input type="hidden" name="f_paymentmethod_id" value="">

									<div id="f_paymentmethod_id" class="flex">
										<input placeholder="Выберите значение" class="flex-1 w-full bg-white focus:outline-none border border-gray-400 focus:border-gray-500 py-1 px-3 appearance-none ss-search-select-result" type="text" value="">
										<div class="bg-gray-200 border border-l-0 border-gray-400 text-gray-600 py-1 px-2 cursor-pointer"><i class="ss-search-deselect fa fa-times"></i></div>
									</div>
								</div>
							</div>
						</div>

						<div class="w-full lg:w-1/2 lg:px-4 lg:ml-auto">
							<div class="md:flex md:items-center mb-4">
								<div class="md:w-2/3 md:ml-auto">
									<button type="submit" class="inline-block rounded px-3 py-2 text-sm leading-4 bg-blue-600 hover:bg-blue-700 focus:outline-none text-white cursor-pointer" title="Фильтровать"><i class="fa fa-filter" aria-hidden="true"></i>&nbsp;<span>Фильтровать</span></button>
									&nbsp;
									<button onclick="location.assign('https://sitename.ru/admin/orders');" type="button" class="inline-block rounded px-3 py-2 text-sm leading-4 bg-blue-600 hover:bg-blue-700 focus:outline-none text-white cursor-pointer" title="Очистить форму">
										<i class="fa fa-times" aria-hidden="true"></i>&nbsp;<span>Очистить форму</span>
									</button>
								</div>
							</div>
						</div>
					</div>
				</form>
			</div>

			<!-- таблица -->
			<div class="p-3">
				<div class="overflow-x-auto pb-2 -mb-2">
					<table class="min-w-full text-sm">
						<thead>
							<tr>
								<th class="p-2 border-b border-gray-300 bg-gray-100" width="30"></th>
								<th class="p-2 border-b border-gray-300 bg-gray-100 text-left align-top leading-4 font-medium text-gray-700"><a href="#" class="underline hover:text-gray-900">Код</a></th>
								<th class="p-2 border-b border-gray-300 bg-gray-100 text-left align-top leading-4 font-medium text-gray-700 whitespace-no-wrap"><a href="#" class="underline hover:text-gray-900">Опубликовано</a>&nbsp;<span class="fa fa-angle-down"></span></th>
								<th class="p-2 border-b border-gray-300 bg-gray-100 text-left align-top leading-4 font-medium text-gray-700"><a href="#" class="underline hover:text-gray-900">Сумма</a></th>
								<th class="p-2 border-b border-gray-300 bg-gray-100 text-left align-top leading-4 font-medium text-gray-700"><a href="#" class="underline hover:text-gray-900">Статус заказа</a></th>
								<th class="p-2 border-b border-gray-300 bg-gray-100 text-left align-top leading-4 font-medium text-gray-700"><a href="#" class="underline hover:text-gray-900">Метод доставки</a></th>
								<th class="p-2 border-b border-gray-300 bg-gray-100 text-left align-top leading-4 font-medium text-gray-700"><a href="#" class="underline hover:text-gray-900">Метод оплаты</a></th>
								<th class="p-2 border-b border-gray-300 bg-gray-100 text-left align-top leading-4 font-medium text-gray-700"><a href="#" class="underline hover:text-gray-900">Заказчик</a></th>
								<th class="p-2 border-b border-gray-300 bg-gray-100 text-left align-top leading-4 font-medium text-gray-700"><a href="#" class="underline hover:text-gray-900">Телефон заказчика</a></th>
								<th class="p-2 border-b border-gray-300 bg-gray-100 text-left align-top leading-4 font-medium text-gray-700"><a href="#" class="underline hover:text-gray-900">E-mail заказчика</a></th>
								<th class="p-2 border-b border-gray-300 bg-gray-100 text-left align-top leading-4 font-medium text-gray-700">Адрес доставки</th>
								<th class="p-2 border-b border-gray-300 bg-gray-100 text-left align-top leading-4 font-medium text-gray-700"><a href="#" class="underline hover:text-gray-900">Дата и время доставки</a</th> <th class="p-2 border-b border-gray-300 bg-gray-100 text-left align-top leading-4 font-medium text-gray-700">Комментарий</th>
								<th class="p-2 border-b border-gray-300 bg-gray-100"></th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td class="px-2 pl-0 py-1 border-b border-gray-300 leading-4">
									<a href="#" class="inline-block rounded px-2 py-1 bg-gray-300 hover:bg-gray-400" title="Показать"><i class="fa fa-eye"></i></a>
								</td>
								<td class="px-2 py-1 border-b border-gray-300 text-right leading-4">
									<a href="#" class="underline text-blue-600 hover:text-blue-800">1592</a>
								</td>
								<td class="px-2 py-1 border-b border-gray-300 leading-4">18.05.2020 14:00</td>
								<td class="px-2 py-1 border-b border-gray-300 text-right leading-4">3&nbsp;582.00&nbsp;р.</td>
								<td class="px-2 py-1 border-b border-gray-300 leading-4 bg-red-600">
									<a href="#" class="underline text-white">Новый</a>
								</td>
								<td class="px-2 py-1 border-b border-gray-300 leading-4">
									<a href="#" class="underline text-blue-600 hover:text-blue-800">Курьером</a>
								</td>
								<td class="px-2 py-1 border-b border-gray-300 leading-4">
									<a href="#" class="underline text-blue-600 hover:text-blue-800">Карта</a>
								</td>
								<td class="px-2 py-1 border-b border-gray-300 leading-4">
									Марина
								</td>
								<td class="px-2 py-1 border-b border-gray-300 leading-4 whitespace-no-wrap">
									+7 (910) 386-35-15
								</td>
								<td class="px-2 py-1 border-b border-gray-300 leading-4">
									kmv141110@gmail.com
								</td>
								<td class="px-2 py-1 border-b border-gray-300 leading-4">
									Нижний Новгород, ул Володарского, дом 38а (красная дверь)
								</td>
								<td class="px-2 py-1 border-b border-gray-300 leading-4">
									18.05.2020 16:00
								</td>
								<td class="px-2 py-1 border-b border-gray-300 leading-4">
									На месте с 9.00 до 11.00
								</td>

								<td class="px-2 pr-0 py-1 border-b border-gray-300 text-right whitespace-no-wrap leading-4">
									<a href="#" class="inline-block rounded px-2 py-1 bg-blue-600 hover:bg-blue-700 text-white" title="Печатать" target="_blank">
										<i class="fa fa-print"></i>
									</a>
									<a onclick="" href="javascript: void(0);" class="inline-block rounded px-2 py-1 bg-blue-600 hover:bg-blue-700 text-white" data-toggle="tooltip" title="Изменить">
										<i class="fa fa-pencil"></i> Изменить
									</a>
									<button onclick="" class="inline-block rounded px-2 py-1 bg-red-600 hover:bg-red-700 text-white focus:outline-none" data-toggle="tooltip" title="Удалить">
										<i class="fa fa-trash"></i>
									</button>
								</td>
							</tr>
							<tr>
								<td class="px-2 pl-0 py-1 border-b border-gray-300 leading-4">
									<a href="#" class="inline-block rounded px-2 py-1 bg-gray-300 hover:bg-gray-400" data-toggle="tooltip" title="Показать"><i class="fa fa-eye"></i></a>
								</td>
								<td class="px-2 py-1 border-b border-gray-300 text-right leading-4">
									<a href="#" class="underline text-blue-600 hover:text-blue-800">1592</a>
								</td>
								<td class="px-2 py-1 border-b border-gray-300 leading-4">18.05.2020 14:00</td>
								<td class="px-2 py-1 border-b border-gray-300 text-right leading-4">3&nbsp;582.00&nbsp;р.</td>
								<td class="px-2 py-1 border-b border-gray-300 leading-4 bg-yellow-300">
									<a href="#" class="underline text-gray-900">В работе</a>
								</td>
								<td class="px-2 py-1 border-b border-gray-300 leading-4">
									<a href="#" class="underline text-blue-600 hover:text-blue-800">Самовывоз</a>
								</td>
								<td class="px-2 py-1 border-b border-gray-300 leading-4">
									<a href="#" class="underline text-blue-600 hover:text-blue-800">Наличные</a>
								</td>
								<td class="px-2 py-1 border-b border-gray-300 leading-4">
									Марина
								</td>
								<td class="px-2 py-1 border-b border-gray-300 leading-4">
									+7 (910) 386-35-15
								</td>
								<td class="px-2 py-1 border-b border-gray-300 leading-4">
									kmv141110@gmail.com
								</td>
								<td class="px-2 py-1 border-b border-gray-300 leading-4">
									Нижний Новгород, ул Володарского, дом 38а (красная дверь)
								</td>
								<td class="px-2 py-1 border-b border-gray-300 leading-4">
									18.05.2020 16:00
								</td>
								<td class="px-2 py-1 border-b border-gray-300 leading-4">
									Нет
								</td>

								<td class="px-2 pr-0 py-1 border-b border-gray-300 text-right whitespace-no-wrap leading-4">
									<a href="#" class="inline-block rounded px-2 py-1 bg-blue-600 hover:bg-blue-700 text-white" title="Печатать" target="_blank">
										<i class="fa fa-print"></i>
									</a>
									<a onclick="" href="javascript: void(0);" class="inline-block rounded px-2 py-1 bg-blue-600 hover:bg-blue-700 text-white" data-toggle="tooltip" title="Изменить">
										<i class="fa fa-pencil"></i> Изменить
									</a>
									<button onclick="" class="inline-block rounded px-2 py-1 bg-red-600 hover:bg-red-700 text-white focus:outline-none" title="Удалить">
										<i class="fa fa-trash"></i>
									</button>
								</td>
							</tr>
							<tr>
								<td class="px-2 pl-0 py-1 border-b border-gray-300 leading-4">
									<a href="#" class="inline-block rounded px-2 py-1 bg-gray-300 hover:bg-gray-400" data-toggle="tooltip" title="Показать"><i class="fa fa-eye"></i></a>
								</td>
								<td class="px-2 py-1 border-b border-gray-300 text-right leading-4">
									<a href="#" class="underline text-blue-600 hover:text-blue-800">1592</a>
								</td>
								<td class="px-2 py-1 border-b border-gray-300 leading-4">18.05.2020 14:00</td>
								<td class="px-2 py-1 border-b border-gray-300 text-right leading-4">3&nbsp;582.00&nbsp;р.</td>
								<td class="px-2 py-1 border-b border-gray-300 leading-4 bg-green-600">
									<a href="#" class="underline text-white">Доставлен</a>
								</td>
								<td class="px-2 py-1 border-b border-gray-300 leading-4">
									<a href="#" class="underline text-blue-600 hover:text-blue-800">Курьером</a>
								</td>
								<td class="px-2 py-1 border-b border-gray-300 leading-4">
									<a href="#" class="underline text-blue-600 hover:text-blue-800">Карта</a>
								</td>
								<td class="px-2 py-1 border-b border-gray-300 leading-4">
									Марина
								</td>
								<td class="px-2 py-1 border-b border-gray-300 leading-4">
									+7 (910) 386-35-15
								</td>
								<td class="px-2 py-1 border-b border-gray-300 leading-4">
									kmv141110@gmail.com
								</td>
								<td class="px-2 py-1 border-b border-gray-300 leading-4">
									Нижний Новгород, ул Володарского, дом 38а (красная дверь)
								</td>
								<td class="px-2 py-1 border-b border-gray-300 leading-4">
									18.05.2020 16:00
								</td>
								<td class="px-2 py-1 border-b border-gray-300 leading-4">
									Нет
								</td>

								<td class="px-2 pr-0 py-1 border-b border-gray-300 text-right whitespace-no-wrap leading-4">
									<a href="#" class="inline-block rounded px-2 py-1 bg-blue-600 hover:bg-blue-700 text-white" title="Печатать" target="_blank">
										<i class="fa fa-print"></i>
									</a>
									<a onclick="" href="javascript: void(0);" class="inline-block rounded px-2 py-1 bg-blue-600 hover:bg-blue-700 text-white" data-toggle="tooltip" title="Изменить">
										<i class="fa fa-pencil"></i> Изменить
									</a>
									<button onclick="" class="inline-block rounded px-2 py-1 bg-red-600 hover:bg-red-700 text-white focus:outline-none" title="Удалить">
										<i class="fa fa-trash"></i>
									</button>
								</td>
							</tr>
						</tbody>
					</table>

					<!-- постраничка -->
					<nav class="mt-4 flex items-center justify-center leading-4 text-sm b-pagination">
						<a class="block py-2 px-3 border border-gray-400 hover:bg-gray-300 focus:outline-none" href="#"><span aria-hidden="true">«</span></a>
						<a class="-ml-px block py-2 px-3 border border-gray-400 hover:bg-gray-300 focus:outline-none" href="#">1</a>
						<a class="-ml-px block py-2 px-3 border border-blue-600 bg-blue-600 text-white focus:outline-none" href="#">2</a>
						<a class="-ml-px block py-2 px-3 border border-gray-400 hover:bg-gray-300 focus:outline-none" href="#">3</a>
						<a class="-ml-px block py-2 px-3 border border-gray-400 hover:bg-gray-300 focus:outline-none" href="#"><span aria-hidden="true">»</span></a>
					</nav>
				</div>
			</div>
		</div>
	</main>

	<?php

	require('partials/footer.php');

	?>
</body>

</html>